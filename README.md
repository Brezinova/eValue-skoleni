# Školení - instalace

Během školení budeme využívat programy:

| Program | Verze | Web |
| ------ | ------ |------ |
| SourceTree | 2.0.20.1 | [www.sourcetreeapp.com] [PrSt] |
| Node | 8.0.0 |[nodejs.org] [PrN] |
| plugin less |----- | npm install -g less |
| Git |2.13.0 | stáhne SourceTree při instalaci nebo [git-scm.com] [Git] |
| PHPStorm/WebStorm | 30 dní Trial | [jetbrains.com] [PhpStorm] |
| Visual Studio Code | 1.12.2 | [code.visualstudio.com] [PrVS] |
| Lightshot | ----- | [https://app.prntscr.com/cs/download.html] [PrL] |
| Photoshop / Affinity |  ----- | ----- |

 [PhpStorm]: <https://www.jetbrains.com/phpstorm/download/#section=windows>
 [PrSt]: <https://www.sourcetreeapp.com/>
 [PrN]:<https://nodejs.org/en/>
 [PrL]:<https://app.prntscr.com/cs/download.html>
 [PrVS]:<https://code.visualstudio.com/>
 [Git]:<https://git-scm.com/download/win/>
 

Programy k instalaci naleznete v tomto repozitáři ve složce **start/programy**.

## Instalace programů
Pokud doposud nemáte, hodí se mít vytvořený účet u Google (není povinné) - *evalue.skoleni@gmail.com*

### 1. SourceTree instalace
SourceTree je užitečný program určený pro práci s verzovacím systémem Git. Poskytuje grafické rozhraní a vizualizaci repozitáře. Vhodné pro začátečníky i milovníky ovládání skrz terminál. 

Možnost přihlášení pomocí Google account nebo registrace. Přihlašuji se pomocí google účtu do Atlassian a pokračuji v registraci. 

* *Connect an Account* - není třeba propojit s repozitářem (Skip)
* *SSH* - nyní není třeba, ale kdykoliv se dá doplnit (generování pomocí Pageant v PuTTY)
* *Git nenalezen* - download embded version (stáhnout a nainstalovat Git)
* *Mercurial nenalezen* - download embded version 
**Instalace dokončena**

### 2. Stáhnout repozitář školení

Tento repoziřát se nacházi na adrese https://gitlab.com/Brezinova/eValue-skoleni
**Použijte HTTPS link ke stažení**
V SourceTree : *Clone* ->  vyplnit údaje o repozitáři + místo na disku, kam uložit data

![SourceTree Clone](/start/install-guide/SourceTree-clone.jpg)


### 3. Přihlášení na gitLab + git
Pro zpřístupnění funkcí pracujících s repozitářem je třeba mít zde účet, opět stačí Google. Po přihlášení je **nutné** změnit heslo, jinak vám nedovolí pushovat.

Základní funkce:
> *pull* - stáhne poslední změny z repozitáře
> *commit* - uloží změny do lokálního repozitáře (repo)
> *push* -  odešle všechny commity z lokálního repa na server (pak je vidí všichni)

**Úkol**
> 1. Vytvořte novou branch 
> 2. Vytvořte složku se svým křestním jménem (bez diakritiky a mezer např. Jirina)
> 3.  Do složky vložte obrázek a soubor index.html se základní strukturou stránky 
> 4.  Commit s komentářem, push

### 4. Instalace node + less
V repozitáři naleznete instalcni soubor k node, nainstalujte si jej
Spusťe příkazový řádek a zadejte `node -v` a poté `npm -v`, čímž si zjistíte svou aktuální verzi. 

Nyní nainstalujeme plugin, který je třeba pro překlad z LESS na CSS https://www.npmjs.com/package/less .  Zadejte ``npm install -g less``
Nyní máme nástroj, který umí překládat less do css. Překlad můžeme spouštět pomocí příkazové řádky, my si ale necháme překlad provádět automaticky od editoru.

### 5. Instalace editoru
Výběr editoru je čistě na vás. Každý preferuje jiný - PhPStorm, Atom, Subline Text, ... my budeme nyní používat PhPStorm, kvůli jeho integraci kompilátoru LESS - instalační soubor si stáhněte.

###6a Nastavení kompilace LESS v PhpStorm
Již máme nainstalovaní plugin lessc z předchozích kroků.
* `File -> Settings (Ctrl + Alt + S)`
![PhpStorm nastavení](/start/install-guide/Php_less_to_css.jpg)


### 6b - Nastavení kompilace LESS ve Visual Studiu
Podle návodu si nastavíme kompilaci less do css https://code.visualstudio.com/docs/languages/css
* `Ctrl + Shift + P` -> `Configure Task Runner`
![Visual studio setting](/start/install-guide/MVCode_setting.jpg)
*  `Others` otevřeme tak tasks.json kde můžeme vytvořit příkaz podobně, jako bychom to dělali v příkazové řádce
>// Less configuration
{
    "version": "0.1.0",
    "command": "lessc",
    "isShellCommand": true,
    "args": ["styles.less", "styles.css"]
}
![Visual studio setting](/start/install-guide/MVCode_less.jpg)

`npm run gulp`
