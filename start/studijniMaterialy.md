# Studijní materiály

## LESS

* [http://www.lesscss.cz/] [less] - syntaxe, proměnné, mixiny
* [online kompilátor less to css] [less2css]

[less]: <http://www.lesscss.cz/#-použití-na-klientské-straně>
[less2css]: <http://less2css.org/>

## Git

* [video z prezentace o Git] [gitVideo]
* [obsáhlejší serie videí o git v sourceTree][gitSourceTree]

[gitVideo]: <https://www.superlectures.com/linuxalt2013/git-zjednodusme-zivot-programatorum>
[gitSourceTree]: <https://www.youtube.com/watch?v=UD7PV8auGLg>


## Free vzdělávací kurzy

* [codeAcademy] [cA]
* [Freecodecamp] [freeCode]
* [Udacity.com][Uda] - některé kurzy zdarma

[cA]: <https://www.codecademy.com/learn/all>
[freeCode]: <https://www.freecodecamp.com/>
[Uda]: <https://www.udacity.com/courses/web-development>


## Užitečné stránky
* [vytvoření fontu z ikonek - https://icomoon.io/] (https://icomoon.io/)
* [fontAwesome - ikonový font] (http://fontawesome.io/icons/)
* [linearicons - ikonový font](https://linearicons.com/free)
* [charmap - náhled do ikonového fontu](https://bluejamesbond.github.io/CharacterMap/)
* [rozlišení zařízení - https://mydevice.io](https://mydevice.io/devices/#sortPhab)
* [podpora css v prohlížečích - can i use](http://caniuse.com/#search=transform)
* [generátor SPRITE] (https://instantsprite.com/)
	* [sprite ukázka w3scholl] - (https://www.w3schools.com/css/css_image_sprites.asp)




